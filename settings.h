#ifndef SETTINGS_H
#define SETTINGS_H

#include <QMap>
#include <QString>
#include <QSettings>

class Settings
{
public:
    ~Settings();

    static Settings& getInstance()
    {
        static Settings instance;
        return instance;
    }

    QMap<QString,QString> readSettingsFromFile(QString f);

    typedef QMap<QString,QString> vSettings;

private:
    Settings();
    vSettings vs;
};

#endif // SETTINGS_H
