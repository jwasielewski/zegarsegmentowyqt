#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QShortcut>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void updateColors(QMap<QString,QString>);

public slots:
    void updateClock();
    void reloadSettings();
    void openHelp();
    void reset();

private:
    Ui::MainWindow *ui;
    QTimer *timer;
    QShortcut *settings_sc;
    QShortcut *help_sc;
    QShortcut *reset_colors;
};

#endif // MAINWINDOW_H
