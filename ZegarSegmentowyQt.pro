#-------------------------------------------------
#
# Project created by QtCreator 2013-05-18T13:01:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ZegarSegmentowyQt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    settings.cpp \
    dialoghelp.cpp

HEADERS  += mainwindow.h \
    settings.h \
    dialoghelp.h

FORMS    += mainwindow.ui \
    dialoghelp.ui

RESOURCES += \
    zasoby.qrc
