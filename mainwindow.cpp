#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settings.h"
#include "dialoghelp.h"
#include <ctime>
#include <QFileDialog>
#include <QString>
#include <QMap>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateClock()));
    timer->start(1000);

    settings_sc = new QShortcut(this);
    settings_sc->setKey(QKeySequence(Qt::Key_O));
    connect(settings_sc, SIGNAL(activated()), this, SLOT(reloadSettings()));

    help_sc = new QShortcut(this);
    help_sc->setKey(QKeySequence(Qt::Key_F1));
    connect(help_sc, SIGNAL(activated()), this, SLOT(openHelp()));

    reset_colors = new QShortcut(this);
    reset_colors->setKey(QKeySequence(Qt::Key_R));
    connect(reset_colors, SIGNAL(activated()), this, SLOT(reset()));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete timer;
    delete settings_sc;
    delete help_sc;
    delete reset_colors;
}

void MainWindow::updateColors(QMap<QString, QString> qm)
{
    for(auto e : qm.toStdMap())
    {
        qDebug() << e.second;
        if(e.first == QString("godzinaD"))
            ui->godzinaD->setStyleSheet(e.second);
        else if(e.first == QString("godzinaJ"))
            ui->godzinaJ->setStyleSheet(e.second);
        else if(e.first == QString("minutaD"))
            ui->minutaD->setStyleSheet(e.second);
        else if(e.first == QString("minutaJ"))
            ui->minutaJ->setStyleSheet(e.second);
        else if(e.first == QString("sekundaD"))
            ui->sekundaD->setStyleSheet(e.second);
        else if(e.first == QString("sekundaJ"))
            ui->sekundaJ->setStyleSheet(e.second);
        else if(e.first == QString("dwukropek"))
        {
            ui->label->setStyleSheet(e.second);
            ui->label_2->setStyleSheet(e.second);
        }
    }
}

void MainWindow::updateClock()
{
    time_t t = time(0);
    struct tm tim;
    localtime_s(&tim, &t);

    ui->godzinaD->display(tim.tm_hour / 10);
    ui->godzinaJ->display(tim.tm_hour % 10);

    ui->minutaD->display(tim.tm_min / 10);
    ui->minutaJ->display(tim.tm_min % 10);

    ui->sekundaD->display(tim.tm_sec / 10);
    ui->sekundaJ->display(tim.tm_sec % 10);
}

void MainWindow::reloadSettings()
{
    QString file = QFileDialog::getOpenFileName(this,
                                                tr("Otórz plik"),
                                                "",
                                                tr("Pliki (*.*)"));
    QMap<QString,QString> v = Settings::getInstance().readSettingsFromFile(file);
    reset();
    updateColors(v);
}

void MainWindow::openHelp()
{
    DialogHelp dh;
    dh.exec();
}

void MainWindow::reset()
{
    QString default_css("background-color:black;color:green;");
    ui->godzinaD->setStyleSheet(default_css);
    ui->godzinaJ->setStyleSheet(default_css);
    ui->minutaD->setStyleSheet(default_css);
    ui->minutaJ->setStyleSheet(default_css);
    ui->sekundaD->setStyleSheet(default_css);
    ui->sekundaJ->setStyleSheet(default_css);
    ui->label->setStyleSheet(default_css);
    ui->label_2->setStyleSheet(default_css);
}
