#include "settings.h"

#include <QFile>
#include <QErrorMessage>

Settings::Settings()
{
}

Settings::~Settings()
{
}

QMap<QString, QString> Settings::readSettingsFromFile(QString f)
{
    vs.clear();
    QFile file(f);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QErrorMessage m;
        m.showMessage("Nie można otworzyć pliku!");
        m.exec();
        return vs;
    }

    while(!file.atEnd())
    {
        QString s = file.readLine();
        if(s.contains("godzinaD"))
            vs.insert(QString("godzinaD"),
                      s.right(s.length()-QString("godzinaD").length()-1));
        else if(s.contains("godzinaJ"))
            vs.insert(QString("godzinaJ"),
                      s.right(s.length()-QString("godzinaJ").length()-1));
        else if(s.contains("minutaD"))
            vs.insert(QString("minutaD"),
                      s.right(s.length()-QString("minutaD").length()-1));
        else if(s.contains("minutaJ"))
            vs.insert(QString("minutaJ"),
                      s.right(s.length()-QString("minutaJ").length()-1));
        else if(s.contains("sekundaD"))
            vs.insert(QString("sekundaD"),
                      s.right(s.length()-QString("sekundaD").length()-1));
        else if(s.contains("sekundaJ"))
            vs.insert(QString("sekundaJ"),
                      s.right(s.length()-QString("sekundaJ").length()-1));
        else if(s.contains("dwukropek"))
            vs.insert(QString("dwukropek"),
                      s.right(s.length()-QString("dwukropek").length()-1));
    }

    return vs;
}
